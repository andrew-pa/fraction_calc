package pw.qxczv.fraction_calc.astimpl;

import pw.qxczv.fraction_calc.*;

import java.util.HashMap;

/**
 * Created by s-apalmer on 9/28/2015.
 */
public class MulTerm extends Term {
    public Expression left, right;
    public MulTerm(Expression l, Expression r) {
        left = l;
        right = r;
    }

    @Override
    public String toString() {
        return left.toString() + " * " + right.toString();
    }

    @Override
    public Value evaluate(HashMap<String, Value> context) throws Exception {
        return ((Fraction)left.evaluate(context)).mul((Fraction)right.evaluate(context));
    }
}
