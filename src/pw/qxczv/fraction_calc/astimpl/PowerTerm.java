package pw.qxczv.fraction_calc.astimpl;

import pw.qxczv.fraction_calc.Expression;
import pw.qxczv.fraction_calc.Fraction;
import pw.qxczv.fraction_calc.Term;
import pw.qxczv.fraction_calc.Value;

import java.util.HashMap;

/**
 * Created by s-apalmer on 10/13/2015.
 */
public class PowerTerm extends Term {
    Expression v;
    Expression exp;

    public PowerTerm(Expression V, Expression EXP) {
        v = V;
        exp = EXP;
    }

    @Override
    public Value evaluate(HashMap<String, Value> context) throws Exception {
        Fraction vv = (Fraction)v.evaluate(context);
        Fraction xp = (Fraction)exp.evaluate(context);
        return (Value)vv.pow(xp);
    }
}
