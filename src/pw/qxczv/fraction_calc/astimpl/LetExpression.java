package pw.qxczv.fraction_calc.astimpl;

import pw.qxczv.fraction_calc.Expression;
import pw.qxczv.fraction_calc.Fraction;
import pw.qxczv.fraction_calc.Value;

import java.util.HashMap;

/**
 * Created by s-apalmer on 10/1/2015.
 */
public class LetExpression extends Expression {
    public String id;
    public Expression value;

    public LetExpression(String i, Expression v) {
        id = i;
        value = v;
    }
    @Override
    public String toString() {
        return id + " := " + value.toString();
    }

    @Override
    public Value evaluate(HashMap<String, Value> context) throws Exception {
        Value v = value.evaluate(context);
        context.put(id, v);
        return v;
    }
}
