package pw.qxczv.fraction_calc.astimpl;

import pw.qxczv.fraction_calc.Term;
import pw.qxczv.fraction_calc.Value;

import java.util.HashMap;

/**
 * Created by s-apalmer on 9/29/2015.
 */
public class VariableTerm extends Term {
    public String id;

    public VariableTerm(String t) {
        id = t;
    }

    public class UndefinedVariableException extends Exception {
        String id;
        public UndefinedVariableException(String i) {
            super("Undefined variable " + i);
            id = i;
        }
    }

    @Override
    public boolean equals(Object o) {
        if(o.getClass() == VariableTerm.class) {
            return ((VariableTerm)o).id.equals(id);
        }
        return o.equals(this);
    }

    @Override
    public String toString() {
        return "#"+id;
    }


    public Value evaluate(HashMap<String, Value> context) throws Exception {
        if(!context.containsKey(id)) throw new UndefinedVariableException(id);
        return context.get(id);
    }
}
