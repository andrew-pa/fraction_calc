package pw.qxczv.fraction_calc.astimpl;

import pw.qxczv.fraction_calc.Expression;
import pw.qxczv.fraction_calc.Fraction;
import pw.qxczv.fraction_calc.Value;

import java.util.HashMap;

/**
 * Created by s-apalmer on 9/28/2015.
 */
public class AddExpression extends Expression {
    public Expression left, right;

    public AddExpression(Expression l, Expression r) {
        left = l; right = r;
    }

    @Override
    public String toString() {
        return left.toString() + " + " + right.toString();
    }

    @Override
    public Value evaluate(HashMap<String, Value> context) throws Exception{
        return ((Fraction)left.evaluate(context)).add((Fraction)right.evaluate(context));
    }
}
