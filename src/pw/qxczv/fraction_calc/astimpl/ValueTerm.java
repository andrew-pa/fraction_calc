package pw.qxczv.fraction_calc.astimpl;

import pw.qxczv.fraction_calc.Term;
import pw.qxczv.fraction_calc.Value;

import java.util.HashMap;

/**
 * Created by s-apalmer on 10/8/2015.
 */
public class ValueTerm extends Term {
    public Value v;
    public ValueTerm(Value V) {
        v = V;
    }

    @Override
    public String toString() {
        return v.toString();
    }

    @Override
    public boolean equals(Object o) {
        if(o.getClass() == ValueTerm.class) {
            return ((ValueTerm)o).v.equals(v);
        }
        return o.equals(this);
    }

    @Override
    public Value evaluate(HashMap<String, Value> context) throws Exception {
        return v;
    }
}
