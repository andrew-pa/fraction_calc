package pw.qxczv.fraction_calc.astimpl;

import com.sun.org.apache.xpath.internal.operations.Bool;
import pw.qxczv.fraction_calc.BooleanValue;
import pw.qxczv.fraction_calc.Expression;
import pw.qxczv.fraction_calc.Value;

import java.util.HashMap;

/**
 * Created by s-apalmer on 10/8/2015.
 */
public class BranchExpression extends Expression {
    Expression v;
    Expression then, els;

    public BranchExpression(Expression V, Expression T, Expression E) {
        v = V; then = T; els = E;
    }


    @Override
    public Value evaluate(HashMap<String, Value> context) throws Exception {
        BooleanValue bv = (BooleanValue)v.evaluate(context);
        if(bv.v) return then.evaluate(context);
        else return els.evaluate(context);
    }
}
