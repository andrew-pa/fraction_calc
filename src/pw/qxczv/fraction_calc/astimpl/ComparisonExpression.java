package pw.qxczv.fraction_calc.astimpl;

import pw.qxczv.fraction_calc.BooleanValue;
import pw.qxczv.fraction_calc.Expression;
import pw.qxczv.fraction_calc.Term;
import pw.qxczv.fraction_calc.Value;

import java.util.HashMap;

/**
 * Created by s-apalmer on 10/8/2015.
 */
public class ComparisonExpression extends Expression {
    public enum CompOp {
        Equal, Less, Greater, LessEqual, GreaterEqual, NotEqual
    }
    CompOp op;
    Expression left, right;

    public ComparisonExpression(CompOp OP, Expression L, Expression R) {
        op = OP;
        left = L; right = R;
    }

    @Override
    public Value evaluate(HashMap<String, Value> context) throws Exception {
        Value a = left.evaluate(context);
        Value b = right.evaluate(context);
        boolean r = false;
        switch (op) {
            case Equal: r = a.equals(b); break;
            case NotEqual: r = !a.equals(b); break;
            case Less: r = a.compareTo(b) < 0; break;
            case LessEqual: r = a.compareTo(b) <= 0; break;
            case Greater: r = a.compareTo(b) > 0; break;
            case GreaterEqual: r = a.compareTo(b) >= 0; break;
        }
        return new BooleanValue(r);
    }
}
