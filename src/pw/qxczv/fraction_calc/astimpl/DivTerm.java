package pw.qxczv.fraction_calc.astimpl;

import pw.qxczv.fraction_calc.Expression;
import pw.qxczv.fraction_calc.Fraction;
import pw.qxczv.fraction_calc.Term;
import pw.qxczv.fraction_calc.Value;

import java.util.HashMap;

/**
 * Created by s-apalmer on 9/29/2015.
 */
public class DivTerm extends Term {
    public Expression left, right;
    public DivTerm(Expression l, Expression r) {
        left = l;
        right = r;
    }

    @Override
    public String toString() {
        return left.toString() + " / " + right.toString();
    }

    @Override
    public Value evaluate(HashMap<String, Value> context) throws Exception {
        return ((Fraction)left.evaluate(context)).div((Fraction)right.evaluate(context));
    }
}
