package pw.qxczv.fraction_calc.astimpl;

import pw.qxczv.fraction_calc.Expression;
import pw.qxczv.fraction_calc.Function;
import pw.qxczv.fraction_calc.Value;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by s-apalmer on 10/8/2015.
 */
public class FunctionApplicationExpression extends Expression {

    Expression f;
    ArrayList<Expression> args;

    public FunctionApplicationExpression(Expression F, ArrayList<Expression> A) {
        f = F; args = A;
    }

    @Override
    public String toString() {
        return f.toString() + " $ " + args.toString();
    }


    @Override
    public Value evaluate(HashMap<String, Value> context) throws Exception {
        ArrayList<Value> argvs = new ArrayList<Value>();
        for(int i = 0; i < args.size(); ++i) argvs.add(args.get(i).evaluate(context));
        return ((Function)f.evaluate(context)).apply(context, argvs);
    }
}
