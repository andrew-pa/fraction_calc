package pw.qxczv.fraction_calc.astimpl;

import pw.qxczv.fraction_calc.Expression;
import pw.qxczv.fraction_calc.ListValue;
import pw.qxczv.fraction_calc.Term;
import pw.qxczv.fraction_calc.Value;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by s-apalmer on 10/12/2015.
 */
public class ListValueTerm extends Term {
    List<Expression> exp;

    public ListValueTerm(List<Expression> x) {
        exp = x;
    }

    @Override
    public Value evaluate(HashMap<String, Value> context) throws Exception {
        List<Value> v = new ArrayList<Value>();
        for(int i = 0; i < exp.size(); ++i) {
            v.add(exp.get(i).evaluate(context));
        }
        return new ListValue(v);
    }
}
