package pw.qxczv.fraction_calc;

import pw.qxczv.fraction_calc.astimpl.FunctionApplicationExpression;
import pw.qxczv.fraction_calc.astimpl.ValueTerm;
import pw.qxczv.fraction_calc.astimpl.VariableTerm;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by s-apalmer on 10/8/2015.
 */
public class Function extends Value {
    public ArrayList<String> arg_names;
    public Expression body;
    public Function(ArrayList<String> argnms, Expression bdy) {
        arg_names = argnms;
        body = bdy;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for(int i = 0; i < arg_names.size(); ++i) {
            sb.append(arg_names.get(i) + " ");
        }
        sb.append("| ");
        sb.append(body == null ? "builtin" : body.toString());
        sb.append("]");
        return sb.toString();
    }

    public Value apply(java.util.HashMap<String, Value> ctx, ArrayList<Value> args) throws Exception {
        if(args.size() < arg_names.size()) { //curry the function
            ArrayList<String> new_argnames =
                    new ArrayList<String>(arg_names.subList(args.size(), arg_names.size()));
            ArrayList<Expression> new_args = new ArrayList<Expression>();
            for(int i = 0; i < args.size(); ++i) {
                new_args.add(new ValueTerm(args.get(i)));
            }
            for(int i = 0; i < new_argnames.size(); ++i) {
                new_args.add(new VariableTerm(new_argnames.get(i)));
            }
            return new Function(new_argnames, new FunctionApplicationExpression(new ValueTerm(this), new_args));
        } else {
            for(int i = 0; i < args.size() && i < arg_names.size(); ++i) {
                ctx.put(arg_names.get(i), args.get(i));
            }
            return body.evaluate(ctx);
        }
    }

    @Override
    public int compareTo(Value o) {
        if(o.getClass() == Function.class) {
            Function of = (Function)o;
            if(of.body.equals(body) && of.arg_names.equals(arg_names)) return 0;
        }
        return 1;
    }
}
