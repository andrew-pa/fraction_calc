package pw.qxczv.fraction_calc;

import java.util.HashMap;

/**
 * Created by s-apalmer on 9/24/2015.
 */
public abstract class Expression {
    public abstract Value evaluate(HashMap<String, Value> context) throws Exception;
}
