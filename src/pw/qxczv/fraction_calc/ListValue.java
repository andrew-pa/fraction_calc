package pw.qxczv.fraction_calc;

import java.util.List;

/*
 * Created by s-apalmer on 10/12/2015.
 */
public class ListValue extends Value {
    List<Value> values;

    public ListValue(List<Value> v) {
        values = v;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{ ");
        for(int i = 0; i < values.size(); ++i)
            sb.append(values.get(i).toString() + " ");
        sb.append("}");
        return sb.toString();
    }

    @Override
    public int compareTo(Value o) {
        return -1;
    }
}
