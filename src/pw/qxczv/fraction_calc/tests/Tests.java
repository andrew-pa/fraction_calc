package pw.qxczv.fraction_calc.tests;

import pw.qxczv.fraction_calc.Value;
import pw.qxczv.fraction_calc.astimpl.AddExpression;
import pw.qxczv.fraction_calc.Expression;
import pw.qxczv.fraction_calc.Fraction;
import pw.qxczv.fraction_calc.Parser;
import pw.qxczv.fraction_calc.astimpl.MulTerm;
import pw.qxczv.fraction_calc.astimpl.ValueTerm;

import java.util.HashMap;

/**
 * Created by s-apalmer on 9/28/2015.
 */
public class Tests {

    public static void test_parse_pattern(Parser p, String in, Expression out) throws Exception {
        p.reset(in);
        HashMap<String,Value> context = new HashMap<String,Value>();
        Value v = p.parse().evaluate(context);
        Value xv = out.evaluate(context);
        if(v.equals(xv)) {
            System.out.println("sucesss for " + in);
        } else {
            throw new Exception("failure for " + in);
        }
    }

    public static void test_parse_pattern(Parser p, String in, Fraction out) throws Exception {
        test_parse_pattern(p, in, new ValueTerm(out));
    }


    public static void run_tests() throws Exception {
        if(!((new Fraction(1)).toString().equals("1"))) throw new Exception("failure for Fraction.toString");
        if(!((new Fraction(1,2)).toString().equals("1/2"))) throw new Exception("failure for Fraction.toString");
        if(!((new Fraction(1,2,3)).toString().equals("1_2/3"))) throw new Exception("failure for Fraction.toString");

        Parser p = new Parser("1");

        test_parse_pattern(p, "4", new Fraction(4, 1));
        test_parse_pattern(p, "456", new Fraction(456, 1));
        test_parse_pattern(p, "1/2", new Fraction(1, 2));
        test_parse_pattern(p, "123/456", new Fraction(123, 456));
        test_parse_pattern(p, "1_1/2", new Fraction(1, 1, 2));
        test_parse_pattern(p, "123_456/789", new Fraction(123, 456, 789));

        test_parse_pattern(p, "-4", new Fraction(-4, 1));
        test_parse_pattern(p, "-456", new Fraction(-456, 1));
        test_parse_pattern(p, "-1/2", new Fraction(-1, 2));
        test_parse_pattern(p, "-1_1/2", new Fraction(-1, 1, 2));

        /*test_parse_pattern(p, "1+2", new AddExpression(new Fraction(1), new Fraction(2)));
        test_parse_pattern(p, "1 + 2", new AddExpression(new Fraction(1), new Fraction(2)));
        test_parse_pattern(p, "1 + 2 + 3", new AddExpression(new AddExpression(new Fraction(1), new Fraction(2)), new Fraction(3)));
        test_parse_pattern(p, "1/2 + 2/3", new AddExpression(new Fraction(1,2), new Fraction(2,3)));
        test_parse_pattern(p, "1_1/2 + 2_3/5", new AddExpression(new Fraction(1,1,2), new Fraction(2,3,5)));

        test_parse_pattern(p, "6 * 7", new MulTerm(new Fraction(6), new Fraction(7)));
        test_parse_pattern(p, "6 * 7 + 2", new AddExpression(new MulTerm(new Fraction(6), new Fraction(7)), new Fraction(2)));
        test_parse_pattern(p, "2 + 6 * 7", new AddExpression(new Fraction(2),new MulTerm(new Fraction(6), new Fraction(7))));*/


    }
}
