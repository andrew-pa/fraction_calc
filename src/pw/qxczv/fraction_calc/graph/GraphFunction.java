package pw.qxczv.fraction_calc.graph;

import pw.qxczv.fraction_calc.Function;
import pw.qxczv.fraction_calc.Value;

import java.util.ArrayList;

/**
 * Created by s-apalmer on 10/12/2015.
 */
public class GraphFunction extends Function {

    public GraphFunction() {
        super(new ArrayList<String>(), null);
        arg_names.add("f");
    }

    @Override
    public Value apply(java.util.HashMap<String, Value> ctx, ArrayList<Value> args) throws Exception {
        Function f = (Function)args.get(0);
        GraphWindow w = new GraphWindow(ctx, f);
        w.setVisible(true);
        w.repaint(10);
        return new Value() {
            @Override
            public int compareTo(Value o) {
                return 0;
            }

            @Override
            public String toString() {
                return "{window}";
            }
        };
    }
}