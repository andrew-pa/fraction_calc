package pw.qxczv.fraction_calc.graph;

import pw.qxczv.fraction_calc.Fraction;
import pw.qxczv.fraction_calc.Function;
import pw.qxczv.fraction_calc.Value;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by s-apalmer on 10/12/2015.
 */
public class GraphWindow extends JFrame {
    Function f;
    java.util.HashMap<String, Value> context;
    public GraphWindow(java.util.HashMap<String, Value> ctx, Function F) {
        super("Graph of " + F.toString());
        f = F;
        context = ctx;
        super.setSize(600, 600);
    }
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D)g;
        int hw = this.getWidth()/2;
        int hh = this.getHeight()/2;
        g2d.translate(hw, hh);
        g2d.setColor(Color.blue);
        g2d.drawLine(-hw, 0, hw, 0);
        g2d.drawLine(0, -hh, 0, hh);
        ArrayList<Value> args = new ArrayList<>();
        args.add(0, null);
        g2d.setColor(Color.black);
        try {

            args.set(0, new Fraction(-hw));
            int lastx = -hw, lasty = -((Fraction)f.apply(context, args)).whole();
            for (int i = -hw; i < hw; ++i) {
                args.set(0, new Fraction(i));
                Fraction y = null;
                try {
                    y = (Fraction) f.apply(context, args);
                } catch(ArithmeticException e) {
                    continue;
                }
                int px = i;
                int py = -y.whole();
                g2d.drawLine(px, py, lastx, lasty);
                lastx = px; lasty = py;
            }
        } catch(Exception e) { e.printStackTrace(); super.setVisible(false); }
    }
}
