package pw.qxczv.fraction_calc;

import pw.qxczv.fraction_calc.astimpl.*;

import java.text.CharacterIterator;
import java.util.ArrayList;

/**
 * Created by s-apalmer on 9/24/2015.
 */
public class Parser {
    String inp;
    int i;

    public Parser(String tinp) {
        inp = tinp;
        i = 0;
    }

    public void reset(String tinp) {
        inp = tinp;
        i = 0;
    }

    //int nextWhitespace(int x) {
    //    while(x < inp.length() && !Character.isWhitespace(inp.charAt(x))) x++;
    //    return x;
    //}

    char nextChar() {
        if(!moreCharp()) return '\0';
        return inp.charAt(i++);
    }

    char peekChar() {
        if((i+1) > inp.length()) return '\0';
        return inp.charAt(i+1);
    }

    void nextWhitespace() {
        while(moreCharp() && Character.isWhitespace(currChar())) i++;
    }

    char currChar() {
        if(!moreCharp()) return '\0';
        return inp.charAt(i);
    }

    boolean endOfTokenp() {
        char c = currChar();
        return !moreCharp() || Character.isWhitespace(c)
                ||  c == '+' || c == '-' || c == '*' || c == '/' || c == '$' || c == '(' || c == ')' || c == ':' || c == ']' || c == ',' || c == '}';
    }

    boolean moreCharp() {
        return i < inp.length();
    }

    Expression parseTerm() throws Exception {
        Expression exp = null;
        nextWhitespace();
        if(currChar() == '(') {
            nextChar();
            exp = parse();
            nextWhitespace();
        } else if(currChar() == '?') {
            nextChar();
            Expression v = parse();
            nextChar();
            Expression t = parse();
            nextChar();
            Expression e = parse();
            exp = new BranchExpression(v, t, e);
            nextWhitespace();
        } else if(currChar() == '{') {
            nextChar();
            ArrayList<Expression> x = new ArrayList<>();
            while(moreCharp() && currChar() != '}') {
                x.add(parse());
                nextWhitespace();
            }
            nextChar();
            exp = new ListValueTerm(x);
            nextWhitespace();
        } else if(Character.isDigit(currChar()) || currChar() == '-' && Character.isDigit(peekChar())) {
                StringBuffer part = new StringBuffer();
                do {
                    part.append(nextChar());
                } while(Character.isDigit(currChar()));
                if(currChar() == '/') {
                    nextChar();
                    int numr = Integer.parseInt(part.toString());
                    part = new StringBuffer();
                    do {
                        part.append(nextChar());
                    } while(Character.isDigit(currChar()));
                    int demr = Integer.parseInt(part.toString());
                    exp = new ValueTerm(new Fraction(numr, demr));
                } else if (currChar() == '_') {
                    nextChar();
                    int whl = Integer.parseInt(part.toString());
                    part = new StringBuffer();
                    do {
                        part.append(nextChar());
                    } while(Character.isDigit(currChar()));
                    int numr = Integer.parseInt(part.toString());
                    nextChar();
                    part = new StringBuffer();
                    do {
                        part.append(nextChar());
                    } while(Character.isDigit(currChar()));
                    int demr = Integer.parseInt(part.toString());
                    exp = new ValueTerm(new Fraction(whl, numr, demr));
                } else exp = new ValueTerm(new Fraction(Integer.parseInt(part.toString())));
                nextWhitespace();
            } else if (currChar() == '[') {
                nextChar();
                ArrayList<String> arg_names = new ArrayList<String>();
                while(currChar() != '|' && moreCharp()) {
                    nextWhitespace();
                    StringBuffer part = new StringBuffer();
                    do part.append(nextChar());
                    while(!Character.isWhitespace(currChar()) && currChar() != '|' && moreCharp());
                    arg_names.add(part.toString());
                    nextWhitespace();
                }
                nextChar();
                nextWhitespace();
                exp = new ValueTerm(new Function(arg_names, parse()));
            } else if(Character.isAlphabetic(currChar())) {
                StringBuffer part = new StringBuffer();
                do part.append(nextChar());
                while(!endOfTokenp());
                exp = new VariableTerm(part.toString());
                nextWhitespace();
            }

        if(!moreCharp()) return exp;
        if(currChar() == '*') {
            nextChar();
            nextWhitespace();
            Expression r = parse();
            exp = new MulTerm(exp, r);
        } else if(currChar() == '/') {
            nextChar();
            nextWhitespace();
            Expression r = parse();
            exp = new DivTerm(exp, r);
        }
        return exp;
    }

    public Expression parse() throws Exception {
        nextWhitespace();

        Expression exp =  parseTerm();
        while(moreCharp()) {
            nextWhitespace();
            if (!moreCharp()) return exp;
            nextWhitespace();
            if (currChar() == '+') {
                nextChar();
                nextWhitespace();
                Expression r = parse();
                exp = new AddExpression(exp, r);
            } else if (currChar() == '-') {
                nextChar();
                nextWhitespace();
                Expression r = parse();
                exp = new SubExpression(exp, r);
            } else if (exp.getClass() == VariableTerm.class && currChar() == ':' && peekChar() == '=') {
                nextChar();
                nextChar();
                nextWhitespace();
                exp = new LetExpression(((VariableTerm) exp).id, parse());
            } else if(currChar() == '<' || currChar() == '>' || currChar() == '=' || currChar() == '!') {
                char opc = currChar(); nextChar();
                boolean weql = currChar() == '='; if(weql) nextChar();
                ComparisonExpression.CompOp op = ComparisonExpression.CompOp.Equal;
                if(opc == '=') op = ComparisonExpression.CompOp.Equal;
                else if(opc == '!') op = ComparisonExpression.CompOp.NotEqual;
                else if(opc == '<') op = weql ? ComparisonExpression.CompOp.LessEqual : ComparisonExpression.CompOp.Less;
                else if(opc == '>') op = weql ? ComparisonExpression.CompOp.GreaterEqual : ComparisonExpression.CompOp.Greater;
                nextWhitespace();
                return new ComparisonExpression(op, exp, parse());
            } else if (currChar() == '$') {
                nextChar();
                ArrayList<Expression> a = new ArrayList<Expression>();
                while(moreCharp()) {
                    nextWhitespace();
                    a.add(parse());
                    nextWhitespace();
                }
                exp = new FunctionApplicationExpression(exp, a);
            } else if(endOfTokenp()) {
                nextChar();
                return exp;
            } else {
                throw new Exception("Expression invalid");
            }
        }
        return exp;
    }
}
