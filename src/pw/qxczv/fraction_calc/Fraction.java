package pw.qxczv.fraction_calc;

import java.util.HashMap;

/**
 * Created by s-apalmer on 9/24/2015.
 */
public class Fraction extends Value {
    public int numr, demr;

    public Fraction(int w) {
        numr = w;
        demr = 1;
    }

    public Fraction(int n, int d) {
        numr = n;
        demr = d;
    }

    public Fraction(int w, int n, int d) {
        numr = (w < 0 ? -1 : 1) * (n + Math.abs(w)*d);
        demr = d;
    }

    @Override
    public boolean equals(Object b) {
        if(b.getClass() == Fraction.class) {
            Fraction f = (Fraction)b;
            return (numr == f.numr) && (demr == f.demr);
        } else {
            return b.equals(this);
        }
    }

    @Override
    public String toString() {
        reduce();
        int whl = numr/demr;
        int nmr = Math.abs(numr%demr);
        if(nmr == 0) return Integer.toString(whl);
        if(whl == 0) return Integer.toString(numr) + "/" + Integer.toString(demr);
        return Integer.toString(whl) + "_" + Integer.toString(nmr) + "/" + Integer.toString(demr);
    }

    public void reduce() {
        //find gcf
        //divide numr + demr by gcf
        // (15/18) lcm = 3, r=5/6
        int gcd = -1;
        int f = Math.max(numr, demr);
        while(f > 0) {

            if((numr % f) == 0 && (demr % f) == 0) {
                gcd = Math.max(f, gcd);
            }
            f--;
        }

        numr /= gcd;
        demr /= gcd;
    }

    public Fraction inverse() throws ArithmeticException {
        if(numr == 0) throw new ArithmeticException("Divide by zero");
        return new Fraction(demr, numr);
    }

    public Fraction add(Fraction b) {
        if(demr == b.demr) {
            return new Fraction(numr + b.numr, demr);
        } else {
            // (a/b) + (c/d) = (a*d/b*d) + (c*b/d*b) = (a*d+c*b)/(b*d)
            return new Fraction(numr*b.demr+b.numr*demr, demr*b.demr);
        }
    }
    public Fraction sub(Fraction b) {
        return add(new Fraction(-b.numr, b.demr));
    }
    public Fraction mul(Fraction b) {
        return new Fraction(numr*b.numr, demr*b.demr);
    }
    public Fraction div(Fraction b) throws ArithmeticException {
        return mul(b.inverse());
    }
    public Fraction pow(Fraction b) {
        return new Fraction(numr, demr);
    }

    public int whole() {
        return numr/demr;
    }

    public double asDouble() {
        return (double)numr / (double)demr;
    }

    @Override
    public int compareTo(Value o) {
        if(o.getClass() != Fraction.class) return 1;
        return Double.compare(asDouble(), ((Fraction)o).asDouble());
    }
}
