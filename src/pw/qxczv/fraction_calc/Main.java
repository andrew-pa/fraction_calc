package pw.qxczv.fraction_calc;
import pw.qxczv.fraction_calc.astimpl.AddExpression;
import pw.qxczv.fraction_calc.astimpl.MulTerm;
import pw.qxczv.fraction_calc.astimpl.ValueTerm;
import pw.qxczv.fraction_calc.astimpl.VariableTerm;

import java.awt.*;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.*;

public class Main {


    public static void main(String[] args) throws Exception {
        //pw.qxczv.fraction_calc.tests.Tests.run_tests();`
        Scanner inp = new Scanner(System.in);
        Parser p = new Parser("");
        java.util.HashMap<String, Value> context = new java.util.HashMap<String, Value>();
        context.put("graph", new pw.qxczv.fraction_calc.graph.GraphFunction());
        while(true) {
            System.out.print(">");
            String line = inp.nextLine();
            if(line.equals("quit")) break;
            if(line.length() == 0) continue;
            p.reset(line);
            Value res = null;
            try {
                Expression x = p.parse();
                res = x.evaluate(context);
                if(res.getClass() == Fraction.class && ((Fraction)res).demr == 0) throw new ArithmeticException("Divide by zero");
            } catch(ArithmeticException e) {
                System.out.println("error: " + e.getMessage());
                continue;
            } catch(VariableTerm.UndefinedVariableException e) {
                System.out.println("error: " + e.getMessage());
                continue;
            } catch(Exception e) {
                System.out.println("error: Invalid input");
                continue;
            }
            System.out.println("= " + res.toString());
        }
    }
}
