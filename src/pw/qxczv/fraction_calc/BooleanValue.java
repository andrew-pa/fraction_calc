package pw.qxczv.fraction_calc;

/**
 * Created by s-apalmer on 10/8/2015.
 */
public class BooleanValue extends Value {
    public boolean v;
    public BooleanValue(boolean V) {v = V;}

    @Override
    public String toString() {
        return v ? "true" : "false";
    }

    @Override
    public int compareTo(Value o) {
        return o.getClass() == BooleanValue.class ? (((BooleanValue)o).v == v ? 0 : -1) : -1;
    }
}
