fraction_calc
=============

Fraction calculator for APCS

Notes: If you load this directly into bluej (File->Open Non BlueJ then open the src/ dir directly) then make sure you compile both the top level package as well as the astimpl package by opening it up. 

# Extra Features

* Variables
     * Syntax is ___name of the variable___ := ___initial expression___
     * To set a variable again, the syntax is the same
* Functions
    * Functions are also values so naming one is the same as variables above
    * To create a function literal, the syntax is [ ___variables seperated by spaces___ | ___body expression___ ] like [x | x * x] or [x y | x + y]
    * To apply a function, use the $ operator: [x | x*x] $ 3 is 9
    * If you provide less arguments than the function requires, the function will be partially applied: [x y | x*y] $ 3 is [y | [x y | x*y] $ 3,y] or effectively [y | 3*y]
* Graphing
    * function named graph, takes one function as a argument
    * graph $ [x | x*x] will open a window with a graph of x^2 in it
    * graph's x range is [-250, 250] as is it's y range